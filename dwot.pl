#!/usr/bin/perl
use Mojo::Base -base;
use Mojo::UserAgent;
use Mojo::File;
use Mojo::JSON qw(encode_json decode_json true);
use Mojo::IOLoop;
use Mojo::Collection 'c';
use Term::ProgressBar;

my $use_cache = ($ENV{USE_CACHE}) ? 1 : 0;
my $use_bar   = ($ENV{NO_BAR})    ? 0 : 1;

mkdir 'cache' unless -d 'cache';
my $ua = Mojo::UserAgent->new();
my $url = 'https://gvu.duniter.io';
$ua->get($url.'/evoTdC.json' => sub {
        my ($ua, $tx) = @_;
        my $res = $tx->result;
        if ($res->is_success) {
            my $members = c(@{decode_json($res->body)->{node}});
            my %nodes = ();
            my @edges = ();
            my $bar = Term::ProgressBar->new($members->size) if $use_bar;
            $members->each(
                sub {
                    my ($m, $num) = @_;
                    $nodes{$m->{pub}} = {
                        id    => $m->{pub},
                        label => $m->{uid},
                        mass  => 5,
                        color => {
                            background => 'plum',
                            highlight  => {
                                background => 'lightgreen',
                                border     => 'green',
                            }
                        },
                        font => {
                            size => 48
                        }
                    };
                }
            );
            my $certs = c(@{decode_json($res->body)->{link}});
            if (defined $certs) {
                        $certs->each(
                            sub {
                                my ($e, $n) = @_;
                                push @edges, {
                                    from   => $e->{issuer},
                                    to     => $e->{receiver},
                                    width  => 3,
                                    arrows => {
                                        to => {
                                            enabled => true
                                        }
                                    },
                                    timestamp => $e->{validation_time},
                                    date => $e->{validation_date},
                                    color => {
                                        inherit => 'both',
                                    }
                                };
                            }
                        );
            }
            @edges = sort {$a->{timestamp} <=> $b->{timestamp}} @edges;
            map { delete $_->{timestamp} } @edges;
            my $data_nodes = encode_json(\%nodes);
            my $data_edges = encode_json(\@edges);
            my $page = <<EOF;
<!DOCTYPE html>
<html>
    <head>
        <title>Duniter : toile de confiance</title>
        <meta charset="utf-8">
        <link href="vis-network.min.css" rel="stylesheet">
        <style>
            #mynetwork {
              width: 98vw;
              height: 98vh;
            }
            #w3-border {
              border: 1px solid #ccc !important;
              width: 100%;
            }
            #w3-progress-bar {
              color: #000 !important;
              background-color: green !important;
              height: 24px;
              width: 0%;
            }
            #w3-progress-text {
              text-align: center;
              margin-top: -24px;
              height: 24px;
            }
        </style>
        <script src="vis.js"></script>
    </head>
    <body>
        <div id="w3-border">
            <div id="w3-progress-bar"></div>
            <div id="w3-progress-text">0%</div>
        </div>
        <div id="mynetwork"></div>
        <script>
            var n     = $data_nodes;
            var e     = $data_edges;
            var ne    = {};
            var nodes = new vis.DataSet([]);
            var edges = new vis.DataSet([]);
            var container = document.getElementById('mynetwork');
            var total = e.length;
            var data = {
                nodes: nodes,
                edges: edges
            };
            var options = {
                layout: {
                    improvedLayout: true,
                },
                physics: {
                    stabilization: {
                        enabled: true
                    },
                    timestep: 0.1
                }
            };
            var network = new vis.Network(container, data, options);
            console.info('Number of edges: '+e.length);
            function addEdge(ed) {
                setTimeout(function() {
                    var edg = ed.shift();
                    var w = 100 * (total - ed.length)/total;
                    document.getElementById('w3-progress-bar').style.width = w+'%';
                    var w2 = Math.round(100000 * (total - ed.length)/total) / 1000;
                    document.getElementById('w3-progress-text').textContent = w2+'% '+edg.date;
                    console.info('Adding edge');
                    if (edg.from !== undefined && ne[edg.from] === undefined) {
                        nodes.add(n[edg.from]);
                        ne[edg.from] = 1;
                    }
                    if (edg.to !== undefined && ne[edg.to] === undefined) {
                        nodes.add(n[edg.to]);
                        ne[edg.to] = 1;
                    }
                    edges.add(edg);
                    if (ed.length !== 0) {
                        addEdge(ed);
                    }
                }, 250);
            }
            addEdge(e);
        </script>
    </body>
</html>
EOF

             Mojo::File->new('public/index.html')->spurt($page);
        } elsif ($res->is_error) {
            say $res->message;
        }
    }
);

Mojo::IOLoop->start unless Mojo::IOLoop->is_running;
